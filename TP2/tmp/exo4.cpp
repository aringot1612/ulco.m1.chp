#include <iostream>
#include <thread>
#include <cmath>
#include <mutex>
#include <chrono>
#include <iostream>
#include <fstream>

// compilation : g++ -std=c++11 -pthread exo1.cpp -o exo1 

#define N 100000000
std::mutex mutex;
int n = 24;
double sum1 = 0.0;
double sum2 = 0.0;
double sum3 = 0.0;

// fonction calculant la somme des entiers entre i0 et i1
double somme(int i0, int i1) {
  double res=0;
  for (int i=i0+1; i<i1; i++)
     res += i;
  return res;
}

double sumOfSquareRoots(int i0, int i1){
  double res=0;
  for (int i=i0+1; i<i1; i++)
     res += sqrt(i);
  return res;
}

double sumOfSquareRootsByLog(int i0, int i1){
  double res=0;
  for (int i=i0+1; i<i1; i++)
     res += sqrt(i)*log(i);
  return res;
}

void exec(int i0, int i1){
  mutex.lock();
  std::cout << "somme - res = " << somme(i0, i1) << std::endl;
  std::cout << "somme racines - res = " << sumOfSquareRoots(i0, i1) << std::endl;
  std::cout << "somme (racines x log) - res = " << sumOfSquareRootsByLog(i0, i1) << std::endl;
  mutex.unlock();
}

void sommes(int i0, int i1){
  sum1 += somme(i0, i1);
  sum2 += sumOfSquareRoots(i0, i1);
  sum3 += sumOfSquareRootsByLog(i0, i1);
}

int main(int argc, char *argv[]){
  std::ofstream myfile;
  myfile.open ("example.csv");
  // Simple thread
  auto startSeq = std::chrono::system_clock::now();
  exec(0, N);
  auto endSeq = std::chrono::system_clock::now();
  std::cout<<"Temps de calcul séquentiel = "<<((std::chrono::duration<double>)(endSeq-startSeq)).count()<<std::endl;

  // 3 threads
  std::thread myThreads3[3];
  myThreads3[0] = std::thread(somme, 0, N);
  myThreads3[1] = std::thread(sumOfSquareRoots, 0, N);
  myThreads3[2] = std::thread(sumOfSquareRootsByLog, 0, N);
  auto startThread = std::chrono::system_clock::now();
  for (int i=0; i<3; i++){
    myThreads3[i].join();
  }
  auto endThread = std::chrono::system_clock::now();
  std::cout<<"Temps de calcul multi-thread 3 = "<<((std::chrono::duration<double>)(endThread-startThread)).count()<<std::endl;

  // 24 threads
  std::thread myThreads24[n];
  for (int i=0; i<n; i++){
    myThreads24[i] = std::thread(sommes, (N/n)*i, (N/n)*(i+1));
  }
  auto startThread2 = std::chrono::system_clock::now();
  for (int i=0; i<n; i++){
    myThreads24[i].join();
  }
  auto endThread2 = std::chrono::system_clock::now();
  std::cout<<"Temps de calcul multi-thread 24 = "<<((std::chrono::duration<double>)(endThread2-startThread2)).count()<<std::endl;
  std::cout << "somme - res = " << sum1 << std::endl;
  std::cout << "somme racines - res = " << sum2 << std::endl;
  std::cout << "somme (racines x log) - res = " << sum3 << std::endl;
  return 1;
}