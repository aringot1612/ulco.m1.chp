#include <iostream>
#include <thread>
#include <cmath>
#include <mutex>

// compilation : g++ -std=c++11 -pthread exo1.cpp -o exo1 

#define N 10000000
std::mutex mutex;

// fonction calculant la somme des entiers entre i0 et i1
void somme(int i0, int i1) {
  double res=0;
  for (int i=i0; i<i1; i++)
     res += i;
}

int main(int argc, char *argv[]){
  std::cout << "Début du main " << std::endl;

  // lancer les threads
  std::thread myThreads[24];
  for (int i=0; i<24; i++){
    myThreads[i] = std::thread(somme, i, i);
  }

  std::cout << "Milieu du main " << std::endl;

  for (int i=0; i<24; i++){
    myThreads[i].join();
  }

  std::cout << "Fin du main" << std::endl;

  std::cout << "Nombre de coeurs :: "<< std::thread::hardware_concurrency() << std::endl;
  return 1;
}