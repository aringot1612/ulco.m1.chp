#include <iostream>
#include <thread>
#include <cmath>
#include <mutex>
#include <chrono>

// compilation : g++ -std=c++11 -pthread exo1.cpp -o exo1 

#define N 100000000
std::mutex mutex;

// fonction calculant la somme des entiers entre i0 et i1
void somme(int i0, int i1) {
  double res=0;
  for (int i=i0+1; i<i1; i++)
     res += i;
  mutex.lock();
  std::cout << "somme - res = " << res << std::endl;
  mutex.unlock();
}

void sumOfSquareRoots(int i0, int i1){
  double res=0;
  for (int i=i0+1; i<i1; i++)
     res += sqrt(i);
  mutex.lock();
  std::cout << "somme racines - res = " << res << std::endl;
  mutex.unlock();
}

void sumOfSquareRootsByLog(int i0, int i1){
  double res=0;
  for (int i=i0+1; i<i1; i++)
     res += sqrt(i)*log(i);
  mutex.lock();
  std::cout << "somme (racines x log) - res = " << res << std::endl;
  mutex.unlock();
}

void exec(int i0, int i1){
  mutex.lock();
  std::cout<< "Démarrage du thread = "<<std::this_thread::get_id()<<std::endl;
  mutex.unlock();
  somme(i0, i1);
  sumOfSquareRoots(i0, i1);
  sumOfSquareRootsByLog(i0, i1);
}

int main(int argc, char *argv[]){
  std::cout << "Début du main " << std::endl;
  int n = 3;

  auto startSeq = std::chrono::system_clock::now();
  exec(0, N);
  auto endSeq = std::chrono::system_clock::now();
  std::cout<<"Temps de calcul séquentiel = "<<((std::chrono::duration<double>)(endSeq-startSeq)).count()<<std::endl;

  // lancer les threads
  std::thread myThreads[n];
  myThreads[0] = std::thread(somme, 0, N);
  myThreads[1] = std::thread(sumOfSquareRoots, 0, N);
  myThreads[2] = std::thread(sumOfSquareRootsByLog, 0, N);
  /*for (int i=0; i<n; i++){
    myThreads[i] = std::thread(somme, 0, N);
  }
  std::cout << "Milieu du main " << std::endl;
  */

  auto startThread = std::chrono::system_clock::now();
  for (int i=0; i<n; i++){
    myThreads[i].join();
  }
  auto endThread = std::chrono::system_clock::now();

  std::cout<<"Temps de calcul multi-thread = "<<((std::chrono::duration<double>)(endThread-startThread)).count()<<std::endl;
  std::cout << "Fin du main" << std::endl;
  return 1;
}