#include <iostream>
#include <thread>
#include <cmath>
#include <mutex>
#include <chrono>
#include <iostream>
#include <fstream>
#include <cstdlib>

#define N 100000000
std::mutex mutex;
double sum1 = 0.0;
double sum2 = 0.0;
double sum3 = 0.0;

double somme(int i0, int i1) {
  double res=0;
  for (int i=i0+1; i<i1; i++)
     res += i;
  return res;
}

double sumOfSquareRoots(int i0, int i1){
  double res=0;
  for (int i=i0+1; i<i1; i++)
     res += sqrt(i);
  return res;
}

double sumOfSquareRootsByLog(int i0, int i1){
  double res=0;
  for (int i=i0+1; i<i1; i++)
     res += sqrt(i)*log(i);
  return res;
}

void sommes(int i0, int i1){
  double tmpRes1 = 0.0;
  double tmpRes2 = 0.0;
  double tmpRes3 = 0.0;

  tmpRes1 = somme(i0, i1);
  tmpRes2 = sumOfSquareRoots(i0, i1);
  tmpRes3 = sumOfSquareRootsByLog(i0, i1);

  mutex.lock();
  sum1 += tmpRes1;
  mutex.unlock();
  mutex.lock();
  sum2 += tmpRes2;
  mutex.unlock();
  mutex.lock();
  sum3 += tmpRes3;
  mutex.unlock();
}

int main(int argc, char *argv[]){
  std::ofstream myFile("data.txt");
  for(int n = 1; n <= 48 ; n++){
    sum1 = 0.0;
    sum2 = 0.0;
    sum3 = 0.0;
    std::thread myThreads[n];
    for (int i=0; i<n; i++){
      myThreads[i] = std::thread(sommes, (N/n)*i, (N/n)*(i+1));
    }
    auto startThread = std::chrono::system_clock::now();
    for (int i=0; i<n; i++){
      myThreads[i].join();
    }
    auto endThread = std::chrono::system_clock::now();
    std::cout << sum1 << " - " << sum2 << " - " << sum3 << std::endl;
    myFile << n << " threads : " << ((std::chrono::duration<double>)(endThread-startThread)).count() <<  " secondes\n";
  }
  myFile.close();
  return 1;
}