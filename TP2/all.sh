#!/bin/sh
BIN="./exo4_run"

rm -r build
mkdir build
cd build
cmake ..
cmake --build . --config Release --target exo4_run

${BIN}