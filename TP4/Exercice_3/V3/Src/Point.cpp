#include "Point.hpp"


ostream& operator<<(ostream& out, const Point& p){
  out << "(" << p.X << "," << p.Y << ",";
  out << p.Z << ")";
  return out;
}

Point Point::operator-(const Point& p){
  return Point(X-p.X, Y-p.Y, Z-p.Z);
}

Point Point::operator*(const Point& p){
  return Point(X*p.X, Y*p.Y, Z*p.Z);
}