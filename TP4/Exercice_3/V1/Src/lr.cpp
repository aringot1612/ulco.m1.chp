#include <iostream>
#include <chrono> 
using namespace std;

//#include "Sphere.hpp"
//#include "Plan.hpp"
#include "Scene.hpp"
#include "Vecteur.hpp"
#include "Image.hpp"
#include "Camera.hpp"

// profondeur de la récursion pour le lancer de rayons
#define PROF 2

int main(int argc, char *argv[]){
  // vérification de la syntaxe de la commande
  if(argc !=4){
    cerr << "ERREUR - Syntaxe = ";
    cerr << argv[0] << " <fichier descriptif d'une scène>";
    cerr << endl;
    return -1;
  }

  // création et chargement de la scène
  Scene sc;
  if(!sc.charger(argv[1])){
    cerr << "ERREUR au chargement du fichier " << argv[1] << endl;
    return -1;
  };

  // création de l'image et de la caméra
  Image im(atoi(argv[2]),atoi(argv[2]));
  Camera cam;

  // génération de l'image à partir de la caméra
  auto start = std::chrono::high_resolution_clock::now();
  cam.genererImageParallele(sc, im, PROF, atoi(argv[3]));
  cout << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count() << ";";
  return 1;
}