/******************************************************

 Author: Arthur Ringot.

 ******************************************************/

#include "Triangle.hpp"
#include "Plan.hpp"

#define SP_EPSILON 0.0001
//#define CURLESS 0
#define MOLLER 0

Triangle::Triangle() : Objet(){
  s[0].set(-1, 0, -1);
  s[1].set(1, 0, -1);
  s[2].set(0, 0, 1);
  n.set(0, 0, 1);
}

Triangle::Triangle(const Point p[3], Materiau m) : Objet(m) {
  for(int i=0; i<3; i++)
    s[i] = p[i];
  Vecteur AB(p[0], p[1]);
  Vecteur AC(p[0], p[2]);
  Vecteur vectorialProduct = AB.vectorialProduct(AC);
  vectorialProduct.normaliser();
  n = vectorialProduct;
}

Triangle::~Triangle(){}

#ifdef CURLESS
bool Triangle::intersecte(const Rayon& r, Intersection& inter){
  float nd = n * r.direction;
  float d = n*s[0];
  float t = ((d-(n * r.origine))/(nd));
  if(t < SP_EPSILON) return false;
  Point Q(r.origine.X + t*r.direction.dx, r.origine.Y + t*r.direction.dy, r.origine.Z + t*r.direction.dz);

  Vecteur ba(s[1], s[0]);
  Vecteur qa(Q, s[0]);
  Vecteur cb(s[2], s[1]);
  Vecteur qb(Q, s[1]);
  Vecteur ac(s[0], s[2]);
  Vecteur qc(Q, s[2]);

  if(n*(ba.vectorialProduct(qa))>=0 && n*(cb.vectorialProduct(qb))>=0 && n*(ac.vectorialProduct(qc))>=0){
    inter = Intersection(Q, this, t);
    return true;
  }
  else
    return false;
}

bool Triangle::coupe(const Rayon& r){
  float nd = n * r.direction;
  float d = n*s[0];
  float t = ((d-(n * r.origine))/(nd));
  if(t < SP_EPSILON) return false;
  Point Q(r.origine.X + t*r.direction.dx, r.origine.Y + t*r.direction.dy, r.origine.Z + t*r.direction.dz);

  Vecteur ba(s[1], s[0]);
  Vecteur qa(Q, s[0]);
  Vecteur cb(s[2], s[1]);
  Vecteur qb(Q, s[1]);
  Vecteur ac(s[0], s[2]);
  Vecteur qc(Q, s[2]);

  if(n*(ba.vectorialProduct(qa))>=0 && n*(cb.vectorialProduct(qb))>=0 && n*(ac.vectorialProduct(qc))>=0)
    return true;
  else
    return false;
}
#endif

#ifdef MOLLER
bool Triangle::intersecte(const Rayon& r, Intersection& inter){
  Vecteur ab(s[0], s[1]);
  Vecteur ac(s[0], s[2]);
  Vecteur orig = r.origine;
  Vecteur dir = r.direction;
  Vecteur pvec = dir.vectorialProduct(ac);

  float det = ab*pvec;
  if (det > -SP_EPSILON && det < SP_EPSILON) return false;

  float inv_det = 1.0/det;

  Vecteur tvec = orig - s[0];
  float u = (tvec*pvec) * inv_det;
  if(u < SP_EPSILON || u > 1.0) return false;

  Vecteur qvec = tvec.vectorialProduct(ab);
  float v = (dir*qvec) * inv_det;
  if(v < SP_EPSILON || (u+v) > 1.0) return false;

  float t = (ac*qvec) * inv_det;
  if(t < SP_EPSILON) return false;
  float w = 1 - u - v;

  Point Q(w*s[0].X + u*s[1].X + v*s[2].X, w*s[0].Y + u*s[1].Y + v*s[2].Y, w*s[0].Z + u*s[1].Z + v*s[2].Z);
  inter = Intersection(Q, this, t);

  return true;
}

bool Triangle::coupe(const Rayon& r){
  Vecteur ab(s[0], s[1]);
  Vecteur ac(s[0], s[2]);
  Vecteur orig = r.origine;
  Vecteur dir = r.direction;
  Vecteur pvec = dir.vectorialProduct(ac);

  float det = ab*pvec;
  if (det > -SP_EPSILON && det < SP_EPSILON) return false;

  float inv_det = 1.0/det;

  Vecteur tvec = orig - s[0];
  float u = (tvec*pvec) * inv_det;
  if(u < SP_EPSILON || u > 1.0) return false;

  Vecteur qvec = tvec.vectorialProduct(ab);
  float v = (dir*qvec) * inv_det;
  if(v < SP_EPSILON || (u+v) > 1.0) return false;

  float t = (ac*qvec) * inv_det;
  if(t < SP_EPSILON) return false;

  return true;
}
#endif

Vecteur Triangle::getNormale(const Point &p){
  return n;
}

ostream& operator<<(ostream & sortie, Triangle & t){
  sortie << "triangle : ";
  for(int i=0; i<3; i++)
    sortie << t.s[i] << " - ";
  sortie << endl;

  return sortie;
}

void Triangle::affiche(ostream& out){
  out << "triangle : ";
  for(int i=0; i<3; i++)
    out << s[i] << " - ";
  out << endl;
}