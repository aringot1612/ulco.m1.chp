#!/bin/sh
BIN_V0="./lr_v0"
BIN_V1="./lr_v1"
BIN_V2="./lr_v2"
BIN_V3="./lr_v3"
BIN_V4="./lr_v4"
SCENE="../Scenes/scene03.txt"
CSVFILE="results.csv"
DIM_LIST="128 256 512 1024 2048"
THREAD_LIST="2 4 8 16 24"

rm -r build
mkdir build
cd build

cmake ..
cmake --build . --config Release --target lr_v0
cmake --build . --config Release --target lr_v1
cmake --build . --config Release --target lr_v2
cmake --build . --config Release --target lr_v3
cmake --build . --config Release --target lr_v4
clear && clear

printf "\n\nLR mono-thread (Scene 03)" >> ${CSVFILE}
printf "\nRésolution :;128x128;256x256;512x512" >> ${CSVFILE}
printf "\n ;" >> ${CSVFILE}
for nDim in ${DIM_LIST} ; do
	${BIN_V0} ${SCENE} ${nDim} >> ${CSVFILE}
done
printf "\n\nLR (Scene 03) avec parallélisation statique de la boucle externe" >> ${CSVFILE}
printf "\nRésolution :;128x128;256x256;512x512;1024x1024;2048x2048" >> ${CSVFILE}
for nThread in ${THREAD_LIST} ; do
	printf "\n${nThread} threads;" >> ${CSVFILE}
	for nDim in ${DIM_LIST} ; do
		${BIN_V1} ${SCENE} ${nDim} ${nThread} >> ${CSVFILE}
	done
done

printf "\n\nLR (Scene 03) avec parallélisation statique de la boucle interne" >> ${CSVFILE}
printf "\nRésolution :;128x128;256x256;512x512;1024x1024;2048x2048" >> ${CSVFILE}
for nThread in ${THREAD_LIST} ; do
	printf "\n${nThread} threads;" >> ${CSVFILE}
	for nDim in ${DIM_LIST} ; do
		${BIN_V2} ${SCENE} ${nDim} ${nThread} >> ${CSVFILE}
	done
done

printf "\n\nLR (Scene 03) avec parallélisation statique des deux boucles" >> ${CSVFILE}
printf "\nRésolution :;128x128;256x256;512x512;1024x1024;2048x2048" >> ${CSVFILE}
for nThread in ${THREAD_LIST} ; do
	printf "\n${nThread} threads;" >> ${CSVFILE}
	for nDim in ${DIM_LIST} ; do
		${BIN_V3} ${SCENE} ${nDim} ${nThread} >> ${CSVFILE}
	done
done

printf "\n\nLR (Scene 03) avec parallélisation dynamique" >> ${CSVFILE}
printf "\nnRésolution :;128x128;256x256;512x512;1024x1024;2048x2048" >> ${CSVFILE}
for nThread in ${THREAD_LIST} ; do
	printf "\n${nThread} threads;" >> ${CSVFILE}
	for nDim in ${DIM_LIST} ; do
		${BIN_V4} ${SCENE} ${nDim} ${nThread} >> ${CSVFILE}
	done
done