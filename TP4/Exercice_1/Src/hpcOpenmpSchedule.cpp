#include <cmath>
#include <fstream>
#include <iostream>
#include <thread>
#include <vector>
#include <omp.h>
#include <string>

// Programme de visualisation de la répartition des tâches de
// calcul sous OpenMP
// auteur : J. Dehos

#ifndef M_PI
  #define M_PI 3.14
#endif

// output data as a grayscale PNM image
void writePnm(std::ostream &os, int width, int height, const std::vector<unsigned char> & data)
{
    os << "P2" << std::endl;
    os << width << ' ' << height << std::endl;
    os << "255" << std::endl;
    for (unsigned char pixel : data) os << (unsigned)pixel << ' ';
}

void writeInfile(std::string name, int width, int height, const std::vector<unsigned char> & data){
    std::ofstream ofs(name);
    writePnm(ofs, width, height, data);
}

int main(int argc, char ** argv)
{
    // check command line arguments
    if (argc!=5 && argc!=6)
    {
        std::cout << "usage; " << argv[0] << " <width> <height> [NB Thread] [N Sims] [withImage ?]\n";
        return -1;
    }
    // reserve image data
    int width = std::stoi(argv[1]);
    int height = std::stoi(argv[2]);
    int NBT = std::stoi(argv[3]);
    int nSims = std::atoi(argv[4]);
    bool withImage = false;
    if(argc == 6)
        withImage = true;
    std::vector<unsigned char> data(width*height);
    auto ind = [&data,width](int xx, int yy)->unsigned char&
    { return data[yy*width+xx]; };

    double startTime = omp_get_wtime();
    double sumTimes1 = 0;
    double sumTimes2 = 0;
    double sumTimes3 = 0;
    double sumTimes4 = 0;
    double sumTimes5 = 0;
    double sumTimes6 = 0;

    for(int i = 0 ; i < nSims ; i++){
        //Pas de parallélisation
        startTime = omp_get_wtime();
        for (int x=width-1; x>=0; x--)
        {
            for (int y=height-1; y>=0; y--)
            {
                double t = (x+y) / sqrt(width*width + height*height);
                double f = 2.0;
                ind(x,y) = 127.0 * (1 + cos(2.0*M_PI*f*t));
            }
        }
        sumTimes1 += (omp_get_wtime() - startTime);
        if(i == 0 && withImage)
            writeInfile("img-"+ std::to_string(NBT)+"_threads-type1.pnm", width, height, data);

        //Parallélisation simple externe
        startTime = omp_get_wtime();
        #pragma omp parallel for num_threads(NBT)
        for (int x=width-1; x>=0; x--)
            for (int y=height-1; y>=0; y--)
                ind(x,y) = 255-(255/(omp_get_thread_num()+1))*omp_get_thread_num();
        sumTimes2 += (omp_get_wtime() - startTime);
        if(i == 0 && withImage)
            writeInfile("img-"+ std::to_string(NBT)+"_threads-type2.pnm", width, height, data);

        //Parallélisation statique externe (50)
        startTime = omp_get_wtime();
        #pragma omp parallel for num_threads(NBT) schedule(static, 50)
        for (int x=width-1; x>=0; x--)
            for (int y=height-1; y>=0; y--)
                ind(x,y) = 255-(255/(omp_get_thread_num()+1))*omp_get_thread_num();
        sumTimes3 += (omp_get_wtime() - startTime);
        if(i == 0 && withImage)
            writeInfile("img-"+ std::to_string(NBT)+"_threads-type3.pnm", width, height, data);

        //Parallélisation statique interne (50)
        startTime = omp_get_wtime();
        for (int x=width-1; x>=0; x--){
            #pragma omp parallel for num_threads(NBT) schedule(static, 50)
            for (int y=height-1; y>=0; y--){
                ind(x,y) = 255-(255/(omp_get_thread_num()+1))*omp_get_thread_num();
            }
        }
        sumTimes4 += (omp_get_wtime() - startTime);
        if(i == 0 && withImage)
            writeInfile("img-"+ std::to_string(NBT)+"_threads-type4.pnm", width, height, data);

        //Parallélisation statique interne et externe (taille de bloc : 50)
        startTime = omp_get_wtime();
        #pragma omp parallel for num_threads(NBT) schedule(static, 50) collapse(2)
        for (int x=width-1; x>=0; x--)
            for (int y=height-1; y>=0; y--)
                ind(x,y) = 255-(255/(omp_get_thread_num()+1))*omp_get_thread_num();
        sumTimes5 += (omp_get_wtime() - startTime);
        if(i == 0 && withImage)
            writeInfile("img-"+ std::to_string(NBT)+"_threads-type5.pnm", width, height, data);

        //Parallélisation statique interne et externe (taille de bloc : 32)
        startTime = omp_get_wtime();
        #pragma omp parallel for num_threads(NBT) schedule(static, 32) collapse(2)
        for (int x=width-1; x>=0; x--)
            for (int y=height-1; y>=0; y--)
                ind(x,y) = 255-(255/(omp_get_thread_num()+1))*omp_get_thread_num();
        sumTimes6 += (omp_get_wtime() - startTime);
        if(i == 0 && withImage)
            writeInfile("img-"+ std::to_string(NBT)+"_threads-type6.pnm", width, height, data);
    }
    std::cout << (sumTimes1/nSims) << ";" << (sumTimes2/nSims) << ";" << (sumTimes3/nSims) << ";" << (sumTimes4/nSims) << ";" << (sumTimes5/nSims) << ";" << (sumTimes6/nSims) << ";";
    return 0;
}