#!/bin/sh
BIN="./hpcOpenmpSchedule"
LARGEUR="400"
HAUTEUR="300"
CSVFILE="results.csv"
DIM_LIST="512 1024 2048"
THREAD_LIST="2 4 8 16 24"
N_SIMS="100"

rm -r build
mkdir build
cd build
cmake ..
cmake --build . --config Release --target hpcOpenmpSchedule

for nDim in ${DIM_LIST} ; do
	printf "\n\nImage en ${nDim}x${nDim}pxl" >> ${CSVFILE}
	printf "\nType;Pas de parallélisation;Parallélisation simple externe;Parallélisation statique externe (50);Parallélisation statique interne (50);Parallélisation statique interne et externe (50);Parallélisation statique interne et externe (32);" >> ${CSVFILE}
	for nThread in ${THREAD_LIST} ; do
		printf "\n${nThread} threads;" >> ${CSVFILE}
		${BIN} ${nDim} ${nDim} ${nThread} ${N_SIMS} >> ";" >> ${CSVFILE}
	done
done