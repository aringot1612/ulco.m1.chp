#include <iostream>
#include <thread>
#include <cmath>
#include <mutex>
#include <chrono>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <omp.h>

#define N 100000000

double sum1 = 0;
double sum2 = 0;
double sum3 = 0;

void sommes(int i0, int i1, int NBT){
  #pragma omp parallel num_threads(NBT)
  {
    double tmpSum1 = 0;
    double tmpSum2 = 0;
    double tmpSum3 = 0;
    #pragma omp for schedule(static, i1/NBT)
    for (int i=i0; i<i1; i++){
      tmpSum1 += i;
      tmpSum2 += sqrt(i);
      tmpSum3 += sqrt(i)*log(i);
    }
    #pragma omp atomic
    sum1 += tmpSum1;
    #pragma omp atomic
    sum2 += tmpSum2;
    #pragma omp atomic
    sum3 += tmpSum3;
  }
}

int main(int argc, char *argv[]){
  if(argc != 3){
    std::cout << "Nombre d'arguments insufisant. \nUsage : [nbThread] [nbSims]." << std::endl;
    return -1;
  }
  double nbt = std::atoi(argv[1]);
  double nSims = std::atoi(argv[2]);
  double sumTime = 0;
  for(int i = 0 ; i < nSims ; i++){
    sum1 = 0;
    sum2 = 0;
    sum3 = 0;
    auto startThread = std::chrono::system_clock::now();
    sommes(1, N, nbt);
    sumTime += ((std::chrono::duration<double>)(std::chrono::system_clock::now()-startThread)).count();
  }
  std::cout << (sumTime/nSims);
  return 1;
}