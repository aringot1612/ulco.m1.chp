#!/bin/sh
BIN="./exo4_run"
NSIMS="100"
THREAD_LIST="1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24"
CSVFILE="results.csv"

rm -r build
mkdir build
cd build
cmake ..
cmake --build . --config Release --target exo4_run

for nThread in ${THREAD_LIST} ; do
	printf "\n${nThread} threads;" >> ${CSVFILE}
	${BIN} ${nThread} ${NSIMS} >> ${CSVFILE}
done