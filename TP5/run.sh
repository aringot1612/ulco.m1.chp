#!/bin/sh
BIN_SEQ="./main_sequential"
BIN_PAR_V1="mpirun -np 12 ./main_parallel_v1"
BIN_PAR_V2="mpirun -np 12 ./main_parallel_v2"

mkdir build
cd build
cmake ..
cmake --build . --config Release --target main_sequential
cmake --build . --config Release --target main_parallel_v1
cmake --build . --config Release --target main_parallel_v2
clear && clear

${BIN_SEQ}
${BIN_PAR_V1}
${BIN_PAR_V2}