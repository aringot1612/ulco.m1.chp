#include "../core/monteCarlo.hpp"

class SequentialComputing : public MonteCarlo {
	public:
		SequentialComputing();
		void compute(int n, double precision);
};