#include "sequentialComputing.hpp"

int main(int argc, char *argv[]){
	srand((unsigned int)time(NULL));
	SequentialComputing monteCarlo;
	int n = 100000000;

	/**
	 *  Appel au calcul de PI Monte Carlo.
	 * 
	 * Paramètres :
	 * 
	 * * n : Nombre de points dans le cercle trigo max testées.
	 * 
	 * * precision : Precision de PI à trouver.
	 * */
	monteCarlo.compute(n, 0.000001);
	return 1;
}