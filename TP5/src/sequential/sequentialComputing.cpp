#include "sequentialComputing.hpp"

SequentialComputing::SequentialComputing() {};

void SequentialComputing::compute(int n, double precision){
	auto start = std::chrono::system_clock::now();
	std::vector<std::pair<double, double>> posList = getNList(n);
	int count = 0;
	int pointCount = 0;
	double pi = 0;
	do{
		if(checkIfPointInCircle(posList.at(count))){
			pointCount++;
		}
		count++;
		pi = 4 * (pointCount/(float)n);
	}while (count < n && !checkPIValue(pi, precision));
	std::cout << std::setprecision(16) << "\n\nCalcul séquentiel\nPI trouvé : " << pi << " trouvé en " << count << " occurences et en ";
	std::cout << std::chrono::duration_cast<std::chrono::duration<float>>(std::chrono::system_clock::now() - start).count() << " secondes." << std::endl;
};