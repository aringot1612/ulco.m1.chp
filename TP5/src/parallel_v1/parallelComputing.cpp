#include "parallelComputing.hpp"

ParallelComputing::ParallelComputing() {};

std::pair<int, int> ParallelComputing::findPoints(int n, int maxOccur, double precision){
	std::vector<std::pair<double, double>> posList = getNList(n);
	int count = 0;
	int pointCount = 0;
	do{
		if(checkIfPointInCircle(posList.at(count)))
			pointCount++;
		count++;
	}while (count < maxOccur);
	return std::make_pair(maxOccur, pointCount);
};

void ParallelComputing::compute(int n, int subN, double precision){
	// Initialisation.
	int rank, size, i;
	int indic = 0;
	int finalTotal = 0;
	int finalCount = 0;
	double pi, finalPi, begin;
	std::pair<int, int> tmp;

	MPI_Barrier(MPI_COMM_WORLD);
    begin = MPI_Wtime();
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
	int recvTotal[size];
    int recvCount[size];
	srand((unsigned int)rank);

	// Tant que la précision n'est pas atteinte et que la limite de points n'est pas franchie.
	while(indic != 1){
		// Si le process est un process fils.
		if(rank != 0)
		{
			// Récupèration du nombre de points utilisées et nombre de points dans le cercle sous forme de paire.
			tmp = findPoints(subN, subN, precision);
			// Envoie des valeurs vers process 0.
			MPI_Send(&tmp.first, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
			MPI_Send(&tmp.second, 1, MPI_INT, 0, 2, MPI_COMM_WORLD);
		}
		// Process maître.
		else
		{
			i = 1;
			// Pour chaque process fils existant
			while(i < size && indic != 1){
				// Récupèration des valeurs.
				MPI_Recv(&recvTotal[i], size, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
				MPI_Recv(&recvCount[i], size, MPI_INT, MPI_ANY_SOURCE, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
				// Sommes des valeurs récupérées.
				finalTotal += recvTotal[i];
				finalCount += recvCount[i];
				// Calcul de PI.
				pi = 4 * (finalCount/(float)finalTotal);
				// Condition d'arrêt.
				if(finalTotal >= n || checkPIValue(pi, precision)){
					indic = 1;
					finalPi = pi;
				}
				i++;
			}
		}
		// Mise à jour condition d'arrêt pour tous les process.
		MPI_Bcast(&indic, 1, MPI_INT, 0, MPI_COMM_WORLD);
	}
	// Affichage final.
	if(rank == 0){
		std::cout << std::setprecision(16) << "\n\nCalcul parallèle v1 \nPI trouvé : " << finalPi << " trouvé en " << finalTotal << " occurences et en ";
		std::cout << (MPI_Wtime() - begin)<< " secondes." << std::endl;
	}
};