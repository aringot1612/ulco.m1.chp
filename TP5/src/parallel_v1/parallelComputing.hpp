#include "../core/monteCarlo.hpp"
#include <mpi.h>

class ParallelComputing : public MonteCarlo {
	private:
		std::pair<int, int> findPoints(int n, int maxOccur, double precision);
	public:
		ParallelComputing();
		void compute(int n, int subN, double precision);
};