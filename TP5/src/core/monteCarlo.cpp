#include "monteCarlo.hpp"

MonteCarlo::MonteCarlo() {
	minValue = -1;
	maxValue = 1;
	radius = 1;
};

double MonteCarlo::getRandomValue(){
    double random = ((double) rand()) / (double) RAND_MAX;
    double diff = maxValue - minValue;
    double r = random * diff;
    return minValue + r;
};

std::pair<double, double> MonteCarlo::getRandomPosition(){
	return std::make_pair(getRandomValue(), getRandomValue());
};

bool MonteCarlo::checkIfPointInCircle(std::pair<double, double> pos){
	return (pow(pos.first, 2) + pow(pos.second, 2) < pow(radius, 2));
};

bool MonteCarlo::checkIfPointInCircle(double posX, double posY){
	return (pow(posX, 2) + pow(posY, 2) < pow(radius, 2));
};

std::vector<std::pair<double, double>> MonteCarlo::getNList(int n){
	std::vector<std::pair<double, double>> posList;
	for(int i = 0 ; i < n ; i++){
		posList.push_back(getRandomPosition());
	}
	return posList;
};

bool MonteCarlo::checkPIValue(double value, double precision){
	return (abs(M_PI-value) < precision);
};