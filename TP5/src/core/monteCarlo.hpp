#include <utility>
#include <vector>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <chrono>
#include <random>
#include <functional>
#include <chrono>
#include <math.h>
#include <time.h>

class MonteCarlo{
	protected:
		int radius;
		int minValue;
		int maxValue;
		double getRandomValue();
		bool checkIfPointInCircle(std::pair<double, double> pos);
		bool checkIfPointInCircle(double posX, double posY);
		bool checkPIValue(double value, double precision);
		std::pair<double, double> getRandomPosition();
		std::vector<std::pair<double, double>> getNList(int n);
	public:
		MonteCarlo();
};