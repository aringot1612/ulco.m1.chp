#include "parallelComputing.hpp"

ParallelComputing::ParallelComputing() {};

std::pair<int, int> ParallelComputing::findPoints(double valuesX[], double valuesY[], int maxOccur, double precision){
	int count = 0;
	int pointCount = 0;
	do{
		if(checkIfPointInCircle(valuesX[count], valuesY[count]))
			pointCount++;
		count++;
	}while (count < maxOccur);
	return std::make_pair(maxOccur, pointCount);
};

void ParallelComputing::compute(int n, int subN, double precision){
	// Initialisation.
	int globalRank, globalSize, processRank, processSize, i;
	double pi, finalPi, begin;
	std::vector<std::pair<double, double>> posList;
	std::pair<int, int> tmp = std::make_pair(1, 1);
	int msg;
	double valuesX[subN];
	double valuesY[subN];
	int indic = 0;
	int finalTotal = 1;
	int finalCount = 1;
	int localTotal = 0;
	int localCount = 1;

	MPI_Barrier(MPI_COMM_WORLD);
    begin = MPI_Wtime();
	MPI_Comm_rank(MPI_COMM_WORLD, &globalRank);
    MPI_Comm_size(MPI_COMM_WORLD, &globalSize);
	srand((unsigned int)globalRank);

	// Création du sous-communicateur.
	MPI_Comm auxComm;
	int color;
	if(globalRank > 0)
		color = 1;
	else
		color = 0;
	MPI_Comm_split(MPI_COMM_WORLD, color, globalRank, &auxComm);
	MPI_Comm_rank(auxComm, &processRank);
    MPI_Comm_size(auxComm, &processSize);

	// Tant que la précision n'est pas atteinte et que la limite de points n'est pas franchie.
	while(indic != (-1)){
		// Le process actuel est le processus serveur.
		if(color == 0){
			// Il attend une demande de positions aléatoires.
			MPI_Recv(&msg, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			// Si jamais la valeur reçue est égale à -1, il s'agit du mécanisme d'arrêt pour ce processus.
			if(msg == (-1))
				indic = msg;
			// Sinon, demande de positions aléatoires basique.
			else{
				// Génération de deux tableaux de positions aléatoires.
				for(i = 0 ; i < subN ; i++){
					valuesX[i] = getRandomValue();
					valuesY[i] = getRandomValue();
				}
				// Envoi des deux tableaux de positions aléatoires.
				MPI_Send(&valuesX, subN, MPI_DOUBLE, msg, 1, MPI_COMM_WORLD);
				MPI_Send(&valuesY, subN, MPI_DOUBLE, msg, 2, MPI_COMM_WORLD);
			}
		}
		// Si un processus entre ici, il appartient aux processus de calculs.
		else {
			// On effectue les sommes automatiques des résultats intermédiaires ici, ces sommes seront stockées dans le processus 0 du comm process.
			MPI_Reduce(&localTotal, &finalTotal, 1, MPI_INT, MPI_SUM, 0, auxComm);
			MPI_Reduce(&localCount, &finalCount, 1, MPI_INT, MPI_SUM, 0, auxComm);
			// Si le processus possède le rang global 1, il s'agit du processus effectuant le calcul de PI...
			if(globalRank == 1){
				// Vérification du PI selon les valeurs sommées.
				pi = 4.0 * (finalCount/(float)finalTotal);
				// Vérification de la condition d'arrêt.
				if(finalTotal >= n/* || checkPIValue(pi, precision)*/){
					// Ici, la valeur de PI optimale a été trouvée...
					indic = (-1);
					finalPi = pi;
					// Un dernier message est envoyé au processus serveur, lui indiquant que son travail est terminé.
					MPI_Send(&indic, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
				}
			// Sinon, le processus effectue l'appel au processus serveur pour calculer une somme intermédiaire.
			}else{
				// Envoi d'une demande de positions aléatoires au processus serveur.
				MPI_Send(&globalRank, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
				// Réception des points aléatoires sous la forme de deux tableaux.
				MPI_Recv(&valuesX, subN, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
				MPI_Recv(&valuesY, subN, MPI_DOUBLE, 0, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
				// Récupèration du nombre de points à l'intérieur du cercle trigo.
				tmp = findPoints(valuesX, valuesY, subN, precision);
				// Mise à jour des sommes intermédiaires.
				localTotal += tmp.first;
				localCount += tmp.second;
			}
		}
		// Mise à jour condition d'arrêt pour tous les process du communicateur auxiliaire.
		MPI_Bcast(&indic, 1, MPI_INT, 0, auxComm);
	}
	// Affichage final.
	if(globalRank == 1){
		std::cout << std::setprecision(16) << "\n\nCalcul parallèle v2 \nPI trouvé : " << finalPi << " trouvé en " << finalTotal << " occurences et en ";
		std::cout << (MPI_Wtime() - begin)<< " secondes.\n\n" << std::endl;
	}
};