#include "parallelComputing.hpp"

int main(int argc, char **argv){
	ParallelComputing monteCarlo;
	MPI_Init(&argc, &argv);
	int n = 100000000;

	/**
	 *  Appel au calcul de PI Monte Carlo.
	 * 
	 * Paramètres :
	 * 
	 * * n : Nombre de points dans le cercle trigo max testées.
	 * 
	 * * subN : Nombre de points testées à chaque appel de calcul multi-threadé (MPI)
	 * 
	 * * * Plus subN est grand, plus le calcul aura des chances d'être rapide, au détriment de la précision de PI (un subN trop grand empéchera de trouver PI avant que n soit franchie)...
	 * * * Plus subN est petit, plus le programme sera apte à trouver PI avec une précision suffisante (mais il prendra généralement plus de temps)... 
	 * * * Ce paramètre permet d'affiner la recherche de PI, au détriment du temps de calcul...
	 * 
	 * * precision : Precision de PI à trouver.
	 * */
	monteCarlo.compute(n, 100, 0.000001);
	MPI_Finalize();
}