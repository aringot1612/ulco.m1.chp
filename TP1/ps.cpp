/******************************************************

 Author: Arthur Ringot.

 ******************************************************/

#include <immintrin.h>
#include <iostream>
#include <string>
#include <ctime>

// programme d'évaluation du gain obtenu en utilisant les routines AVX
// pour le calcul de produits scalaires
// utilise les routines AVX et AVX2 (pour les doubles)
// Compilation : g++  -std=c++11 -march=native -O2 ps.cpp -o ps
//     ou      : g++  -std=c++11 -mavx -mavx2 -O2 ps.cpp -o ps


#define NB 10000  // nombre de fois où le produit scalaire est effectué
#define SIZE 80000 // taille des vecteurs utilisés

// -------------------------------------------
// Fonctions d'affichage
// -------------------------------------------
// affiche les réels contenus dans un __m256i, __m256 et __m256d.
void printVal(std::string s, __m256i *v);
void printVal(std::string s, __m256 *v);
void printVal(std::string s, __m256d *v);

// -------------------------------------------
// Fonctions de calcul
// -------------------------------------------
// calcul des produits scalaires sous forme de vecteurs d'entiers
void ps_int(int size);

// calcul des produits scalaires sous forme de vecteurs de float
void ps_float(int size);

// calcul des produits scalaires sous forme de vecteurs de double
void ps_double(int size);

int main(int argc, char*argv[]){
  ps_int(SIZE);
  ps_float(SIZE);
  ps_double(SIZE);
  return 0;
}

void ps_int(int size){
  std::cout << "\n--------------------------------------------------\n" << std::endl;
  clock_t start;

  // initialisation des deux vecteurs entiers
  alignas(32) int *v1, *v2; // alignement sur 32 bits pour AVX

  v1=(int*)_mm_malloc(size*sizeof(int),32);
  v2=(int*)_mm_malloc(size*sizeof(int),32);

  // initialisation avec des valeurs aléatoires dans [0,9]
  for(int i=0; i<size; i++){
    v1[i] = (int)(rand()%10);
    v2[i] = (int)(rand()%10);
  }

  start =  clock();

  int somme;

  for(int n=0; n<NB; n++){
    somme=0;
    for(int i=0; i<size; i++) somme += v1[i]*v2[i];
  }

	float t1;
	std::cout << "\ntemps ps int seq = " << (t1=(clock()-start)/(double)CLOCKS_PER_SEC);
	std::cout << " secondes - valeur = " << somme << std::endl;

  // produit scalaire AVX
  start = clock();

  __m256i _somme;
  __m256i _v1, _v2;

  for(int n=0; n<NB; n++){
    _somme = _mm256_setzero_si256();
    if(n==0) printVal("\nLes 8 valeurs sont :", &_somme); 
    for(int i=0; i<size; i+=8){
        _v1 = _mm256_load_si256((__m256i*)&v1[i]);
        _v2 = _mm256_load_si256((__m256i*)&v2[i]);
        _somme = _mm256_add_epi32(_somme, _mm256_mullo_epi32(_v1, _v2));
    }
  }

  int *p = (int *)&_somme;
  int sommeI = 0;
  for (int i = 0; i < 8; i++)
    sommeI += p[i];

	float t2;
	std::cout << "\ntemps ps int avx = " << (t2=(clock()-start)/(double)CLOCKS_PER_SEC);
	std::cout << " secondes - valeur = " << sommeI << std::endl;
  std::cout << "------Acceleration : " << t1 - t2 << " secondes." << std::endl;

  _mm_free(v1);
  _mm_free(v2);
}

void ps_float(int size){
  std::cout << "\n--------------------------------------------------\n" << std::endl;
  clock_t start;

  // initialisation des deux vecteurs réels
  alignas(32) float *v1, *v2; // alignement sur 32 bits pour AVX

  v1=(float*)_mm_malloc(size*sizeof(float),32);
  v2=(float*)_mm_malloc(size*sizeof(float),32);

  // initialisation avec des valeurs aléatoires dans [0,9]
  for(int i=0; i<size; i++){
    v1[i] = (float)(rand()%10);
    v2[i] = (float)(rand()%10);
  }

  // produit scalaire séquentiel
  start =  clock();

  float somme;
  for(int n=0; n<NB; n++){
    somme=0;
    for(int i=0; i<size; i++) somme += v1[i]*v2[i];
  }

  float t1;
  std::cout << "\ntemps ps float seq = " << (t1=(clock()-start)/(double)CLOCKS_PER_SEC);
  std::cout << " secondes - valeur = " << somme << std::endl;

  // produit scalaire AVX
  start =  clock();

  __m256 _somme;
  __m256 _v1, _v2;

  for(int n=0; n<NB; n++){
    _somme = _mm256_setzero_ps();
    if(n==0) printVal("\nLes 8 valeurs sont :", &_somme); 
    for(int i=0; i<size; i+=8){
        _v1 = _mm256_load_ps(&v1[i]);
        _v2 = _mm256_load_ps(&v2[i]);
        _somme = _mm256_add_ps(_somme, _mm256_mul_ps(_v1, _v2));
    }
  }

  float *p = (float *)&_somme;
  float sommeF = 0;
  for (int i = 0; i < 8; i++)
    sommeF += p[i];

	float t2;
	std::cout << "\ntemps ps float avx = " << (t2=(clock()-start)/(double)CLOCKS_PER_SEC);
	std::cout << " secondes - valeur = " << sommeF << std::endl;
  std::cout << "------Acceleration : " << t1 - t2 << " secondes." << std::endl;

  // désallocation mémoire
  _mm_free(v1);
  _mm_free(v2);
}

void ps_double(int size){
  std::cout << "\n--------------------------------------------------\n" << std::endl;
  clock_t start;

  // initialisation des deux vecteurs double
  alignas(32) double *v1, *v2; // alignement sur 32 bits pour AVX

  v1=(double*)_mm_malloc(size*sizeof(double),32);
  v2=(double*)_mm_malloc(size*sizeof(double),32);

  // initialisation avec des valeurs aléatoires dans [0,9]
  for(int i=0; i<size; i++){
    v1[i] = (double)(rand()%10);
    v2[i] = (double)(rand()%10);
  }

  start =  clock();

  double somme;

  for(int n=0; n<NB; n++){
    somme=0;
    for(int i=0; i<size; i++) somme += v1[i]*v2[i];
  }

	float t1;
	std::cout << "\ntemps ps double seq = " << (t1=(clock()-start)/(double)CLOCKS_PER_SEC);
	std::cout << " secondes - valeur = " << somme << std::endl;

  // produit scalaire AVX
  start =  clock();

  __m256d _somme;
  __m256d _v1, _v2;

  for(int n=0; n<NB; n++){
    _somme = _mm256_setzero_pd();
    if(n==0) printVal("\nLes 4 valeurs sont :", &_somme); 
    for(int i=0; i<size; i+=4){
        _v1 = _mm256_load_pd(&v1[i]);
        _v2 = _mm256_load_pd(&v2[i]);
        _somme = _mm256_add_pd(_somme, _mm256_mul_pd(_v1, _v2));
    }
  }

  double *p = (double *)&_somme;
  double sommeD = 0;
  for (int i = 0; i < 4; i++)
    sommeD += p[i];

	float t2;
	std::cout << "\ntemps ps double avx = " << (t2=(clock()-start)/(double)CLOCKS_PER_SEC);
	std::cout << " secondes - valeur = " << sommeD << std::endl;
  std::cout << "------Acceleration : " << t1 - t2 << " secondes.\n\n" << std::endl;

  _mm_free(v1);
  _mm_free(v2);
}

void printVal(std::string s, __m256i *v){
    std::cout<<s<<std::endl;
    int *p = (int*)v;
    for (int i=0 ; i<8 ; i++)
        std::cout<< i << " : " <<  p[i]<<" "<<std::endl;
}

void printVal(std::string s, __m256 *v){
    std::cout<<s<<std::endl;
    float *p = (float*)v;
    for (int i=0 ; i<8 ; i++)
        std::cout<< i << " : " <<  p[i]<<" "<<std::endl;
}

void printVal(std::string s, __m256d *v){
    std::cout<<s<<std::endl;
    double *p = (double*)v;
    for (int i=0 ; i<4 ; i++)
        std::cout<< i << " : " <<  p[i]<<" "<<std::endl;
}