#include "Camera.hpp"
#include "Rayon.hpp"
#include <thread>
#include <mutex>

std::mutex _mutex;

int Camera::largzone;
int Camera::hautzone;

Camera::Camera(){
  position = Point(0.0, 0.0, 2.0);;
  cible = Point(0.0, 0.0, 0.0);
  distance = 2.0;
}

Camera::~Camera(){}

void Camera::genererImage(const Scene& sc, Image& im, int profondeur){
  // Calcul des dimensions d'un pixel par rapport
  // à la résolution de l'image - Les pixels doivent être carrés
  // pour éviter les déformations de l'image.
  // On fixe :
  // - les dimensions en largeur de l'écran seront comprises dans [-1, 1]
  // - les dimensions en hauteur de l'écran soront comprises dans [-H/L, H/L]
  float cotePixel = 2.0/im.getLargeur();
  // Pour chaque pixel
  for(int i=0; i<im.getLargeur(); i++){
    for(int j=0; j<im.getHauteur(); j++){
      // calcul des coordonnées du centre du pixel
      float milieuX = -1 + (i+0.5f)*cotePixel;
      float milieuY =  (float)im.getHauteur()/(float)im.getLargeur() - (j+0.5f)*cotePixel;
      Point centre(milieuX, milieuY, 0);
      // Création du rayon
      Vecteur dir(position, centre);
      dir.normaliser();
      Rayon ray(position, dir);
      // Lancer du rayon primaire
      Intersection inter;
      if(sc.intersecte(ray, inter)){
	      im.setPixel(i, j, inter.getCouleur(sc, position, profondeur));
      }
      else
	      im.setPixel(i, j, sc.getFond());
    }// for j
  }// for i
}

ostream& operator<<(ostream& out, const Camera& c){
  out << " position = " << c.position << " - cible = " << c.cible;
  out <<  " - distance = " << c.distance << flush;
  return out;
}

void Camera::genererImageParallele(const Scene& sc, Image& im, int profondeur, int nbThreads){
  std::vector<std::thread> threadVector;
  for(unsigned int i = 0 ; i < nbThreads ; i++)
    threadVector.push_back(std::thread(&Camera::computeAreas, std::ref(sc), std::ref(im), std::ref(profondeur), std::ref(position)));
  for (unsigned i = 0  ; i < nbThreads ; i++)
    threadVector.at(i).join();
};

zone Camera::zoneSuivante(Image& im){
  int static i, j;
  int height = 0, width = 0;
  zone newZone;

  if(j + Camera::largzone < im.getLargeur())
    width = largzone;
  else
    width = im.getLargeur() - j;

  if(i + Camera::hautzone < im.getHauteur())
    height = hautzone;
  else
    height = im.getHauteur() - i;

  newZone = {i, j, width, height};

  if(j + width < im.getLargeur())
    j = j + width;
  else if (i + height <= im.getHauteur()){
    j = 0;
    i = i + height;
  }

  if(height > 0 && width > 0)
    return newZone;
  else
    return {-1, -1, -1, -1};
};

void Camera::calculeZone(const Scene& sc, Image& im, zone& _zone, int profondeur, Point position){
  float cotePixel = 2.0/im.getHauteur();
  for(int i=_zone.x; i<_zone.hauteur+_zone.x; i++){
    for(int j=_zone.y; j<_zone.largeur+_zone.y; j++){
      // calcul des coordonnées du centre du pixel
      float milieuX = -1 + (i+0.5f)*cotePixel;
      float milieuY =  (float)im.getHauteur()/(float)im.getLargeur() - (j+0.5f)*cotePixel;
      Point centre(milieuX, milieuY, 0);
      // Création du rayon
      Vecteur dir(position, centre);
      dir.normaliser();
      Rayon ray(position, dir);
      // Lancer du rayon primaire
      Intersection inter;
      if(sc.intersecte(ray, inter))
	      im.setPixel(i, j, inter.getCouleur(sc, position, profondeur));
      else
	      im.setPixel(i, j, sc.getFond());
    }// for j
  }// for i
}

void Camera::computeAreas(const Scene& sc, Image& im, int profondeur, Point position){
  auto start = std::chrono::high_resolution_clock::now();
  zone target = zoneSuivante(im);
  int count = 0;
  while(target.x != -1){
    count++;
    calculeZone(sc, im, target, profondeur, position);
    target = zoneSuivante(im);
  };
  //_mutex.lock();
  //std::cout << "Nombre de zones calculés par ce thread : " << count << std::endl;
  //std::cout << "Thread - ID : " << std::this_thread::get_id() << std::endl;
  //std::cout << "Thread - Temps : " << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count() << std::endl;
  //_mutex.unlock();
};