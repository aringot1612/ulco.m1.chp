#include "Camera.hpp"
#include "Rayon.hpp"
#include <thread>
#include <mutex>

std::mutex _mutex;

Camera::Camera(){
  position = Point(0.0, 0.0, 2.0);;
  cible = Point(0.0, 0.0, 0.0);
  distance = 2.0;
}

Camera::~Camera(){}

void Camera::genererImage(const Scene& sc, Image& im, int profondeur){
  // Calcul des dimensions d'un pixel par rapport
  // à la résolution de l'image - Les pixels doivent être carrés
  // pour éviter les déformations de l'image.
  // On fixe :
  // - les dimensions en largeur de l'écran seront comprises dans [-1, 1]
  // - les dimensions en hauteur de l'écran soront comprises dans [-H/L, H/L]
  float cotePixel = 2.0/im.getLargeur();
  // Pour chaque pixel
  for(int i=0; i<im.getLargeur(); i++){
    for(int j=0; j<im.getHauteur(); j++){
      // calcul des coordonnées du centre du pixel
      float milieuX = -1 + (i+0.5f)*cotePixel;
      float milieuY =  (float)im.getHauteur()/(float)im.getLargeur() - (j+0.5f)*cotePixel;
      Point centre(milieuX, milieuY, 0);
      // Création du rayon
      Vecteur dir(position, centre);
      dir.normaliser();
      Rayon ray(position, dir);
      // Lancer du rayon primaire
      Intersection inter;
      if(sc.intersecte(ray, inter)){
	      im.setPixel(i, j, inter.getCouleur(sc, position, profondeur));
      }
      else
	      im.setPixel(i, j, sc.getFond());
    }// for j
  }// for i
}

ostream& operator<<(ostream& out, const Camera& c){

  out << " position = " << c.position << " - cible = " << c.cible;
  out <<  " - distance = " << c.distance << flush;
  return out;
}

void Camera::genererImageParallele(const Scene& sc, Image& im, int profondeur, int nbThreads){
  std::vector<std::thread> threadVector;
  std::vector<zone> zones = findZones(im, nbThreads);
  for(unsigned int i = 0 ; i < zones.size() ; i++)
    threadVector.push_back(std::thread(&Camera::calculeZone, std::ref(sc), std::ref(im), std::ref(zones[i]), std::ref(profondeur), std::ref(position)));
  for (unsigned i = 0; i < threadVector.size() ; i++)
    threadVector.at(i).join();
};

std::vector<zone> Camera::findZones(Image& im, int nbThreads){
  std::vector<zone> zones;
  if(nbThreads > 1){
    int hauteurByThread = im.getHauteur()/(nbThreads-1);
    for(int i = 0 ; i < nbThreads ; i++){
      if(i != (nbThreads - 1)){
        zones.push_back({0, i*hauteurByThread, im.getLargeur(), hauteurByThread});
      }else{
        hauteurByThread = im.getHauteur()%(nbThreads-1);
        zones.push_back({0, zones.back().y + zones.back().hauteur, im.getLargeur(), hauteurByThread});
      }
    }
  }
  else
    zones.push_back({0, 0, im.getLargeur(), im.getHauteur()});
  return zones;
};

void Camera::calculeZone(const Scene& sc, Image& im, zone& _zone, int profondeur, Point position){
  auto start = std::chrono::high_resolution_clock::now();
  float cotePixel = 2.0/im.getHauteur();
  // Pour chaque pixel
  for(int i=_zone.x; i<_zone.largeur; i++){
    for(int j=_zone.y; j<_zone.hauteur+_zone.y; j++){
      // calcul des coordonnées du centre du pixel
      float milieuX = -1 + (i+0.5f)*cotePixel;
      float milieuY =  (float)im.getHauteur()/(float)im.getLargeur() - (j+0.5f)*cotePixel;
      Point centre(milieuX, milieuY, 0);
      // Création du rayon
      Vecteur dir(position, centre);
      dir.normaliser();
      Rayon ray(position, dir);
      // Lancer du rayon primaire
      Intersection inter;
      if(sc.intersecte(ray, inter))
	      im.setPixel(i, j, inter.getCouleur(sc, position, profondeur));
      else
	      im.setPixel(i, j, sc.getFond());
    }// for j
  }// for i
  //_mutex.lock();
  //std::cout << "\n\nThread - ID : " << std::this_thread::get_id() << std::endl;
  //std::cout << "Thread - Temps : " << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - start).count() << std::endl;
  //_mutex.unlock();
};