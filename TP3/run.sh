#!/bin/sh

BIN_MONOT="./lr_monoT"
BIN_STAT="./lr_statique"
BIN_DYN="./lr_dynamique"
CSVFILE="results.csv"

SCENE_1="../Scenes/scene01.txt"
SCENE_2="../Scenes/scene02.txt"
SCENE_3="../Scenes/scene03.txt"

LZONE_LIST="3 4 5 16 32"
THREAD_LIST="1 2 3 4 8 12 24"
INV_THREAD_LIST="24 12 8 4 3 2 1"
DIM_LIST="128 256 512 1024 2048"
BONUS_DIM="4096"
CDIM_LIST="128 256 512 1024 2048 4096"

rm -r build
mkdir build
cd build

cmake ..
cmake --build . --config Release --target lr_monoT
cmake --build . --config Release --target lr_statique
cmake --build . --config Release --target lr_dynamique
clear && clear

printf "LR mono-thread\n\n" >> ${CSVFILE}
printf "Scene;128x128;256x256;512x512;1024x1024;2048x2048;4096x4096" >> ${CSVFILE}
printf "\nScene 01;" >> ${CSVFILE}
for nDim in ${CDIM_LIST} ; do
	${BIN_MONOT} ${SCENE_1} ${nDim} >> ${CSVFILE}
done
printf "\nScene 02;" >> ${CSVFILE}
for nDim in ${CDIM_LIST} ; do
	${BIN_MONOT} ${SCENE_2} ${nDim} >> ${CSVFILE}
done
printf "\nScene 03;" >> ${CSVFILE}
for nDim in ${CDIM_LIST} ; do
	${BIN_MONOT} ${SCENE_3} ${nDim} >> ${CSVFILE}
done

printf "\n\nLR multi-thread" >> ${CSVFILE}
for nThread in ${THREAD_LIST} ; do
	printf "\n\n${nThread} threads:\n\n" >> ${CSVFILE}
	printf "Scene;128x128;256x256;512x512;1024x1024;2048x2048" >> ${CSVFILE}
	printf "\nScene 01 Statique;" >> ${CSVFILE}
	for nDim in ${DIM_LIST} ; do
		${BIN_STAT} ${SCENE_1} ${nDim} ${nThread} >> ${CSVFILE}
	done
	for nLZone in ${LZONE_LIST} ; do
		printf "\nScene 01 Dynamique (taille de zone : ${nLZone});" >> ${CSVFILE}
		for nDim in ${DIM_LIST} ; do
			${BIN_DYN} ${SCENE_1} ${nDim} ${nThread} ${nLZone} >> ${CSVFILE}
		done
	done
	printf "\nScene 02 Statique;" >> ${CSVFILE}
	for nDim in ${DIM_LIST} ; do
		${BIN_STAT} ${SCENE_2} ${nDim} ${nThread} >> ${CSVFILE}
	done
	for nLZone in ${LZONE_LIST} ; do
		printf "\nScene 02 Dynamique (taille de zone : ${nLZone});" >> ${CSVFILE}
		for nDim in ${DIM_LIST} ; do
			${BIN_DYN} ${SCENE_2} ${nDim} ${nThread} ${nLZone} >> ${CSVFILE}
		done
	done
	printf "\nScene 03 Statique;" >> ${CSVFILE}
	for nDim in ${DIM_LIST} ; do
		${BIN_STAT} ${SCENE_3} ${nDim} ${nThread} >> ${CSVFILE}
	done
	for nLZone in ${LZONE_LIST} ; do
		printf "\nScene 03 Dynamique (taille de zone : ${nLZone});" >> ${CSVFILE}
		for nDim in ${DIM_LIST} ; do
			${BIN_DYN} ${SCENE_3} ${nDim} ${nThread} ${nLZone} >> ${CSVFILE}
		done
	done
done

for nThread in ${INV_THREAD_LIST} ; do
	printf "\nBonus\n\n${nThread} threads:\n\n" >> ${CSVFILE}
	printf "Scene;4096x4096" >> ${CSVFILE}
	printf "\nScene 01 Statique;" >> ${CSVFILE}
	${BIN_STAT} ${SCENE_1} ${BONUS_DIM} ${nThread} >> ${CSVFILE}
	for nLZone in ${LZONE_LIST} ; do
		printf "\nScene 01 Dynamique (taille de zone : ${nLZone});" >> ${CSVFILE}
		${BIN_DYN} ${SCENE_1} ${BONUS_DIM} ${nThread} ${nLZone} >> ${CSVFILE}
	done
	printf "\nScene 02 Statique;" >> ${CSVFILE}
	${BIN_STAT} ${SCENE_2} ${BONUS_DIM} ${nThread} >> ${CSVFILE}
	for nLZone in ${LZONE_LIST} ; do
		printf "\nScene 02 Dynamique (taille de zone : ${nLZone});" >> ${CSVFILE}
		${BIN_DYN} ${SCENE_2} ${BONUS_DIM} ${nThread} ${nLZone} >> ${CSVFILE}
	done
	printf "\nScene 03 Statique;" >> ${CSVFILE}
	${BIN_STAT} ${SCENE_3} ${BONUS_DIM} ${nThread} >> ${CSVFILE}
	for nLZone in ${LZONE_LIST} ; do
		printf "\nScene 03 Dynamique (taille de zone : ${nLZone});" >> ${CSVFILE}
		${BIN_DYN} ${SCENE_3} ${BONUS_DIM} ${nThread} ${nLZone} >> ${CSVFILE}
	done
done