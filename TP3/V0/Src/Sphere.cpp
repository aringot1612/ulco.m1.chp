/******************************************************

 Author: Arthur Ringot.

 ******************************************************/

#include "Sphere.hpp"
#include <cmath>

#define SP_EPSILON 0.0001

Sphere::Sphere() : Objet(){
  rayon = 1.0;
}


Sphere::Sphere(float xc, float yc, float zc, float r, Materiau m) : Objet(m), centre(xc, yc, zc) {
  rayon = r;
}

Sphere::~Sphere(){}

bool Sphere::intersecte(const Rayon& r, Intersection& inter){
  // Permet de simplifier les calculs.
  float xd = r.direction.dx; 
  float yd = r.direction.dy; 
  float zd = r.direction.dz;
  float x0 = r.origine.X;
  float y0 = r.origine.Y; 
  float z0 = r.origine.Z;
  float xc = centre.X; 
  float yc = centre.Y; 
  float zc = centre.Z;

  // Composantes a, b et c.
  float a = xd*xd + yd*yd + zd*zd;
  float b = 2*(xd*x0 - xc*xd + yd*y0 - yc*yd + zd*z0 - zc*zd);
  float c = x0*x0 + xc*xc - 2*xc*x0 + y0*y0 + yc*yc - 2*yc*y0 + z0*z0 + zc*zc - 2*zc*z0 - rayon*rayon;
  
  // Stock les t.
  float t, t1, t2 = 0.0;

  // Calcul de delta.
  float delta = b*b - 4*a*c;

  // Si delta négatif, pas d'intersection.
  if(delta<0)
    return false;
  else
    // Une seule intersection.
    if (delta == 0){
      t = -b/(2*a);
      // On s'assure que l'intersection ne soit pas derrière l'origine du rayon.
      if (t < SP_EPSILON)
        return false;
      inter = Intersection(Point(x0+xd*t,y0+yd*t,z0+zd*t), this, t);
      return true;
    }
    // Deux intersections.
    else {
      t1= (-b-sqrt(delta))/(2*a);
      t2= (-b+sqrt(delta))/(2*a);
      // Si l'intersection la plus proche de l'origine n'est pas derrière l'origine du rayon.
      if (t1 < t2 && t1 >SP_EPSILON)
        t=t1;
      else if (t2 < t1 && t2 >SP_EPSILON)
        t=t2;
      // Sinon, on ignore les intersections.
      else
        return false;

      inter = Intersection(Point(x0+xd*t,y0+yd*t,z0+zd*t), this, t);
      return true;
    }
}

bool Sphere::coupe(const Rayon& r){
  // Permet de simplifier les calculs.
  float xd = r.direction.dx; 
  float yd = r.direction.dy; 
  float zd = r.direction.dz;
  float x0 = r.origine.X;
  float y0 = r.origine.Y; 
  float z0 = r.origine.Z;
  float xc = centre.X; 
  float yc = centre.Y; 
  float zc = centre.Z;

  // Composantes a, b et c.
  float a = xd*xd + yd*yd + zd*zd;
  float b = 2*(xd*x0 - xc*xd + yd*y0 - yc*yd + zd*z0 - zc*zd);
  float c = x0*x0 + xc*xc - 2*xc*x0 + y0*y0 + yc*yc - 2*yc*y0 + z0*z0 + zc*zc - 2*zc*z0 - rayon*rayon;
  
  // Stock les t.
  float t, t1, t2 = 0.0;

  // Calcul de delta.
  float delta = b*b - 4*a*c;

  // Si delta négatif, pas d'intersection.
  if(delta<0)
    return false;
  else
    // Une seule intersection.
    if (delta == 0){
      t = -b/(2*a);
      // On s'assure que l'intersection ne soit pas derrière l'origine du rayon.
      if (t < SP_EPSILON)
        return false;
      return true;
    }
    // Deux intersections.
    else {
      t1= (-b-sqrt(delta))/(2*a);
      t2= (-b+sqrt(delta))/(2*a);
      // Si l'intersection la plus proche de l'origine n'est pas derrière l'origine du rayon.
      if (t1 < t2 && t1 >SP_EPSILON)
        t=t1;
      else if (t2 < t1 && t2 >SP_EPSILON)
        t=t2;
      // Sinon, on ignore les intersections.
      else
        return false;
      return true;
    }
}

ostream& operator<<(ostream & sortie, Sphere & s){
  // affichage de l'équation de la sphère
  sortie << "Sphere : de rayon " << s.rayon << ", de centre ";
  sortie << s.centre;
  // affichage du matériau de la sphère
  Objet *po = &s;
  sortie << *po << flush;
  return sortie;
}

Vecteur Sphere::getNormale(const Point &p){
  // la normale à la sphère au point P est égale
  // au vecteur CP, avec C le centre de la sphère
  Vecteur n(p.X-centre.X,p.Y-centre.Y, p.Z-centre.Z);
  n.normaliser();
  return n;
}


void Sphere::affiche(ostream& out) {
  out << "Sphere : de rayon " << rayon << ", de centre ";
  out << centre;
  Objet::affiche(out);
}

